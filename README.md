# Spark 2, DataframeReader and DataFrameWriter options #

## Task 1. Playing with JSON DataframeReader options, primitives as String ##

Let's have following Json on the HDFS:

    [root@sandbox ~]# cat people.json 
    {"name": "John", "age": 28, "country": "UK"}
    {name: "Cathy", age: 30, country: "AUS"}
    {"name": "Mark", "age": 50, "country": "USA"}

* There are unquoted field names, we need to enabled it.
* Let's say we do not want to inherit the schema by Spark => all primitives should be string.

Solution:

    %spark2

    val peopleJsonDF = sqlContext.read.option("multiLine", true)
                                .option("primitivesAsString", "true")
                                .option("allowComments", true)
                                .option("allowUnquotedFieldNames", true)
                            .json("/tests/people.json");
                            
    peopleJsonDF.printSchema();

    peopleJsonDF.show();

Apache Zeppelin:

    myschema: org.apache.spark.sql.types.StructType = StructType(StructField(age,IntegerType,false), StructField(country,StringType,false), StructField(name,StringType,false))
    peopleJsonDF: org.apache.spark.sql.DataFrame = [age: string, country: string ... 1 more field]
    root
     |-- age: string (nullable = true)
     |-- country: string (nullable = true)
     |-- name: string (nullable = true)
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 28|     UK| John|
    | 30|    AUS|Cathy|
    | 50|    USA| Mark|
    +---+-------+-----+

## Task 2. Playing with JSON DataframeReader options, setting the schema on JSON DataframeReader ##

Same task as in Task 1, but I want to set the schema to DF and not inherite it.

    %spark2

    var myschema = StructType(Array(
        StructField("age", IntegerType, false),
        StructField("country", StringType, false),
        StructField("name", StringType, false)
    ));

    val peopleJsonDF = sqlContext.read.schema(myschema)
	                            .option("multiLine", true)
                                .option("allowComments", true)
                                .option("allowUnquotedFieldNames", true)
                            .json("/tests/people.json");
                            
    peopleJsonDF.printSchema();

    peopleJsonDF.show();

Zeppelin:

    myschema: org.apache.spark.sql.types.StructType = StructType(StructField(age,IntegerType,false), StructField(country,StringType,false), StructField(name,StringType,false))
    peopleJsonDF: org.apache.spark.sql.DataFrame = [age: int, country: string ... 1 more field]
    root
     |-- age: integer (nullable = true)
     |-- country: string (nullable = true)
     |-- name: string (nullable = true)
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 28|     UK| John|
    | 30|    AUS|Cathy|
    | 50|    USA| Mark|
    +---+-------+-----+

## Task 3. Removing duplicates from Dataframe, whole unique rows.

Let's have following input file:

    [root@sandbox ~]# cat duplicates.csv 
    50|USA|Mark
    30|AUS|Cathy
    20|UK|John
    20|UK|John
    [root@sandbox ~]# 

Task create new dataframe with **unique rows**

    %spark2

    var myschema = StructType(Array(
            StructField("age", IntegerType, false),
            StructField("country", StringType, false),
            StructField("name", StringType, false)
    ));

    var csvDF = sqlContext.read.format("csv").
                    schema(myschema).
                    option("header", false).
                    option("sep", "|").
                    load("/tests/duplicates.csv");
                    
    csvDF.show();

    var uniqueDF = csvDF.dropDuplicates();

    uniqueDF.show();

Zeppelin output:

    myschema: org.apache.spark.sql.types.StructType = StructType(StructField(age,IntegerType,false), StructField(country,StringType,false), StructField(name,StringType,false))
    csvDF: org.apache.spark.sql.DataFrame = [age: int, country: string ... 1 more field]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 50|    USA| Mark|
    | 30|    AUS|Cathy|
    | 20|     UK| John|
    | 20|     UK| John|
    +---+-------+-----+
    uniqueDF: org.apache.spark.sql.Dataset[org.apache.spark.sql.Row] = [age: int, country: string ... 1 more field]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 30|    AUS|Cathy|
    | 20|     UK| John|
    | 50|    USA| Mark|
    +---+-------+-----+

## Task 4. Removing duplicates from Dataframe, unique by columns.

Input file:

    50|USA|Mark
    30|AUS|Cathy
    20|UK|John
    20|GBR|John

Task: Remove duplicates, by age.

    %spark2

    var myschema = StructType(Array(
            StructField("age", IntegerType, false),
            StructField("country", StringType, false),
            StructField("name", StringType, false)
    ));

    var csvDF = sqlContext.read.format("csv").
                    schema(myschema).
                    option("header", false).
                    option("sep", "|").
                    load("/tests/duplicates.csv");
                    
    csvDF.show();

    var uniqueDF = csvDF.dropDuplicates("age");

    uniqueDF.show();

Zeppelin:

    myschema: org.apache.spark.sql.types.StructType = StructType(StructField(age,IntegerType,false), StructField(country,StringType,false), StructField(name,StringType,false))
    csvDF: org.apache.spark.sql.DataFrame = [age: int, country: string ... 1 more field]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 50|    USA| Mark|
    | 30|    AUS|Cathy|
    | 20|     UK| John|
    | 20|    GBR| John|
    +---+-------+-----+
    uniqueDF: org.apache.spark.sql.Dataset[org.apache.spark.sql.Row] = [age: int, country: string ... 1 more field]
    +---+-------+-----+
    |age|country| name|
    +---+-------+-----+
    | 20|     UK| John|
    | 50|    USA| Mark|
    | 30|    AUS|Cathy|
    +---+-------+-----+

Best regards

Tomas



